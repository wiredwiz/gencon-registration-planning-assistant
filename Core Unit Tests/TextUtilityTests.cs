﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Org.Edgerunner.Gencon.EventRegistration.Utilities;

namespace Core_Unit_Tests
{
   /// <summary>
   /// Summary description for TextUtilityTests
   /// </summary>
   [TestClass]
   public class TextUtilityTests
   {
      [TestMethod]
      public void CapitalizeKeepsOrdinalsLowerCase()
      {
         Assert.AreEqual("1st Edition", TextUtilities.CapitalizeWords(System.Globalization.CultureInfo.GetCultureInfo("en-US"), "1st edition"));
      }
   }
}
