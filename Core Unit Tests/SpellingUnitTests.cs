﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Org.Edgerunner.Gencon.EventRegistration.Utilities;

namespace Core_Unit_Tests
{
   [TestClass]
   public class SpellingUnitTests
   {
      [TestMethod]
      public void CorrectSentenceObeysCaseAndProperNames()
      {
         var spell = new Spelling(System.Globalization.CultureInfo.GetCultureInfo("en-US"), "en-US.dic");
         Assert.AreEqual("Dungeons & Dragons 2nd edition from TSR",
                         spell.Correct("Dngeons & Drgaons 2nd edition from TSR"));
      }

      [TestMethod]
      public void SpellCheckD101GamesDoesNotMangleTheName()
      {
         var spell = new Spelling(System.Globalization.CultureInfo.GetCultureInfo("en-US"), "en-US.dic");
         Assert.AreEqual("D101 Games", spell.Correct("D101 Games"));
      }

      [TestMethod]
      public void SpellCheckWizKidsDoesNotMangleTheName()
      {
         var spell = new Spelling(System.Globalization.CultureInfo.GetCultureInfo("en-US"), "en-US.dic");
         Assert.AreEqual("WizKids", spell.Correct("Wizkids"));
      }

      [TestMethod]
      public void SpellCheckPowerCoreGamesDoesNotMangleTheName()
      {
         var spell = new Spelling(System.Globalization.CultureInfo.GetCultureInfo("en-US"), "en-US.dic");
         Assert.AreEqual("P.O.W.E.R. Core Games", spell.Correct("P.O.W.E.R. Core Games"));
      }

      [TestMethod]
      public void SpellCheckMechJockDoesNotMangleTheName()
      {
         var spell = new Spelling(System.Globalization.CultureInfo.GetCultureInfo("en-US"), "en-US.dic");
         Assert.AreEqual("MechJock", spell.Correct("MechJock"));
      }

      [TestMethod]
      public void SpellCheckWildFireDoesNotMangleTheName()
      {
         var spell = new Spelling(System.Globalization.CultureInfo.GetCultureInfo("en-US"), "en-US.dic");
         Assert.AreEqual("WildFire", spell.Correct("WildFire"));
      }
   }
}
