﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Org.Edgerunner.Gencon.EventRegistration;

namespace Core_Unit_Tests
{
   [TestClass]
   public class ReaderTests
   {
      [TestMethod]
      public void Read2012EventCatalog()
      {
         var results = EventsReader.Read("Data/20120823013001.csv");
         Assert.AreEqual(8985, results.Count);
      }
   }
}
