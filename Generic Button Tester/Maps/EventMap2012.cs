﻿using System;
using CsvHelper.Configuration;
using Org.Edgerunner.Gencon.EventRegistration.Data;

namespace Generic_Button_Tester.Maps
{
   public sealed class EventMap2012 : CsvClassMap<EventFileListing>
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="EventMap2012"/> class.
      /// </summary>
      public EventMap2012()
      {
         Map(m => m.Id).Name("Game ID");
         Map(m => m.Group).Name("Group");
         Map(m => m.Title).Name("Title");
         Map(m => m.ShortDescription).Name("Short Description");
         Map(m => m.LongDescription).Name("Long Description");
         Map(m => m.EventType).Name("Event Type");
         Map(m => m.GameSystem).Name("Game System");
         Map(m => m.RulesEdition).Name("Rules Edition");
         Map(m => m.MinimumPlayers).Name("Minimum Players");
         Map(m => m.MaximumPlayers).Name("Maximum Players");
         Map(m => m.AgeRequired).Name("Age Required");
         Map(m => m.ExperienceRequired).Name("Experience Required");
         Map(m => m.MaterialsProvided).Name("Materials Provided");
         Map(m => m.StartDateTime).Name("Start Date & Time");
         Map(m => m.Duration).Name("Duration");
         Map(m => m.EndDateTime).Name("End Date & Time");
         Map(m => m.GmNames).Name("GM Names");
         Map(m => m.Website).Name("Website");
         Map(m => m.Email).Name("Email");
         Map(m => m.IsTournament).Name("Tournament?");
         Map(m => m.RoundNumber).Name("Round Number");
         Map(m => m.TotalRounds).Name("Total Rounds");
         Map(m => m.MinimumPlayTime).Name("Minimum Play Time");
         Map(m => m.AttendeeRegistration).Name("Attendee Registration?");
         Map(m => m.Cost).Name("Cost $");
         Map(m => m.Location).Name("Location");
         Map(m => m.RoomName).Name("Room Name");
         Map(m => m.TableNumber).Name("Table Number");
         Map(m => m.SpecialCategory).Name("Special Category");
         Map(m => m.TicketsAvailable).Name("Tickets Available");
         Map(m => m.LastModified).Name("Last Modified");
      }
   }
}
