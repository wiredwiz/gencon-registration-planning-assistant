﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Generic_Button_Tester.Maps;
using Org.Edgerunner.Gencon.EventRegistration;

namespace Generic_Button_Tester
{
   public partial class Form1 : Form
   {
      public Form1()
      {
         InitializeComponent();
      }

      private void button1_Click(object sender, EventArgs e)
      {
         var results = EventsReader.Read("Data/20120823013001.csv");
         MessageBox.Show(results.Count.ToString());
      }
   }
}
