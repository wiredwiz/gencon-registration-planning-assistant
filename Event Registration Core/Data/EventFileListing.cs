﻿using System;

namespace Org.Edgerunner.Gencon.EventRegistration.Data
{
   public class EventFileListing
   {
      public string Id { get; set; }
      public string Group { get; set; }
      public string Title { get; set; }
      public string ShortDescription { get; set; }
      public string LongDescription { get; set; }
      public EventType EventType { get; set; }
      public string GameSystem { get; set; }
      public string RulesEdition { get; set; }
      public int MinimumPlayers { get; set; }
      public int MaximumPlayers { get; set; }
      public string AgeRequired { get; set; }
      public string ExperienceRequired { get; set; }
      public bool MaterialsProvided { get; set; }
      public DateTime StartDateTime { get; set; }
      public double Duration { get; set; }
      public DateTime EndDateTime { get; set; }
      public string GmNames { get; set; }
      public string Website { get; set; }
      public string Email { get; set; }
      public bool IsTournament { get; set; }
      public int RoundNumber { get; set; }
      public int TotalRounds { get; set; }
      public double MinimumPlayTime { get; set; }
      public string AttendeeRegistration { get; set; }
      public double Cost { get; set; }
      public string Location { get; set; }
      public string RoomName { get; set; }
      public string TableNumber { get; set; }
      public string SpecialCategory { get; set; }
      public int TicketsAvailable { get; set; }
      public DateTime LastModified { get; set; }
   }
}