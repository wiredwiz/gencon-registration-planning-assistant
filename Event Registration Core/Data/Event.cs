﻿#region Apache License 2.0

// Copyright 2015 Thaddeus Ryker
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#endregion

using System;
using System.Collections.Generic;
using PostSharp.Patterns.Contracts;
using PostSharp.Patterns.Model;

namespace Org.Edgerunner.Gencon.EventRegistration.Data
{
   [NotifyPropertyChanged]
   public class Event
   {
      public string Group { get; set; }
      [Required] public string Title { get; set; }
      public string ShortDescription { get; set; }
      public string LongDescription { get; set; }
      public EventType EventType { get; set; }
      public string GameSystem { get; set; }
      public string RulesEdition { get; set; }
      public int MinimumPlayers { get; set; }
      public int MaximumPlayers { get; set; }
      public string AgeRequired { get; set; }
      public string ExperienceRequired { get; set; }
      public bool MaterialsProvided { get; set; }
      [Url] public string Website { get; set; }
      [EmailAddress] public string Email { get; set; }
      public bool IsTournament { get; set; }
      public int TotalRounds { get; set; }
      public double MinimumPlayTime { get; set; }
      public string AttendeeRegistration { get; set; }
      public double Cost { get; set; }
      public string SpecialCategory { get; set; }
      public List<Session> Sessions { get; set; }

      #region Nested type: Session

      [NotifyPropertyChanged]
      public class Session
      {
         [Required] public string Id { get; set; }
         [Required] public DateTime StartDateTime { get; set; }
         public double Duration { get; set; }
         [Required] public DateTime EndDateTime { get; set; }
         public List<string> GameMasters { get; set; }
         public int RoundNumber { get; set; }
         [Required] public string Location { get; set; }
         [Required] public string RoomName { get; set; }
         [Required] public string TableNumber { get; set; }
         public int TicketsAvailable { get; set; }
         public DateTime LastModified { get; set; }
      }

      #endregion
   }
}