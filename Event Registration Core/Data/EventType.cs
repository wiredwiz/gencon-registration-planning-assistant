﻿// ReSharper disable InconsistentNaming
namespace Org.Edgerunner.Gencon.EventRegistration.Data
{
   public enum EventType
   {
      AnimeActivities,
      BoardGame,
      NonCollectibleOrTradableCardGame,
      ElectronicGames,
      EntertainmentEvents,
      FilmFest,
      GenConLLCSponsoredEvent,
      HistoricalMiniatures,
      KidsActivities,
      LARP,
      MiniatureHobbyEvents,
      NonHistoricalMiniatures,
      RolePlayingGame,
      RolePlayingGamersAssociation,
      Seminar,
      SpouseActivities,
      TradableCardGame,
      TrueDungeonAdventures,
      Workshop,
      IsleOfMisfitEvents
   }
}