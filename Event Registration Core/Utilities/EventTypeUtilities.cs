﻿using System;
using Org.Edgerunner.Gencon.EventRegistration.Data;

namespace Org.Edgerunner.Gencon.EventRegistration.Utilities
{
   public static class EventTypeUtilities
   {
      public static string Format(EventType type)
      {
         switch (type)
         {
            case EventType.AnimeActivities:
               return "ANI - Anime Activities";
            case EventType.BoardGame:
               return "BGM - Board Game";
            case EventType.NonCollectibleOrTradableCardGame:
               return "CGM - Non-Collectible / Tradable Card Game";
            case EventType.ElectronicGames:
               return "EGM - Electronic Games";
            case EventType.EntertainmentEvents:
               return "ENT - Entertainment Events";
            case EventType.FilmFest:
               return "FLM - Film Fest";
            case EventType.GenConLLCSponsoredEvent:
               return "GEN - Gen Con LLC Sponsored Event!";
            case EventType.HistoricalMiniatures:
               return "HMN - Historical Miniatures";
            case EventType.KidsActivities:
               return "KID - Kids Activities";
            case EventType.LARP:
               return "LRP - LARP";
            case EventType.MiniatureHobbyEvents:
               return "MHE - Miniature Hobby Events";
            case EventType.NonHistoricalMiniatures:
               return "NMN - Non-Historical Miniatures";
            case EventType.RolePlayingGame:
               return "RPG - Role Playing Game";
            case EventType.RolePlayingGamersAssociation:
               return "RPGA - Role Playing Gamers Association!";
            case EventType.Seminar:
               return "SEM - Seminar";
            case EventType.SpouseActivities:
               return "SPA - Spouse Activities";
            case EventType.TradableCardGame:
               return "TCG - Tradable Card Game";
            case EventType.TrueDungeonAdventures:
               return "TDA - True Dungeon Adventures!";
            case EventType.Workshop:
               return "WKS - Workshop";
            case EventType.IsleOfMisfitEvents:
               return "ZED - Isle of Misfit Events";
            default:
               throw new Exception(string.Format("Unable to format event type \"{0}\"", type));
         }
      }

      public static EventType Parse(string text)
      {
         switch (text)
         {
            case "ANI - Anime Activities":
               return EventType.AnimeActivities;
            case "BGM - Board Game":
               return EventType.BoardGame;
            case "CGM - Non-Collectible / Tradable Card Game":
               return EventType.NonCollectibleOrTradableCardGame;
            case "EGM - Electronic Games":
               return EventType.ElectronicGames;
            case "ENT - Entertainment Events":
               return EventType.EntertainmentEvents;
            case "FLM - Film Fest":
               return EventType.FilmFest;
            case "GEN - Gen Con LLC Sponsored Event!":
               return EventType.GenConLLCSponsoredEvent;
            case "HMN - Historical Miniatures":
               return EventType.HistoricalMiniatures;
            case "KID - Kids Activities":
               return EventType.KidsActivities;
            case "LRP - LARP":
               return EventType.LARP;
            case "MHE - Miniature Hobby Events":
               return EventType.MiniatureHobbyEvents;
            case "NMN - Non-Historical Miniatures":
               return EventType.NonHistoricalMiniatures;
            case "RPG - Role Playing Game":
               return EventType.RolePlayingGame;
            case "RPGA - Role Playing Gamers Association!":
               return EventType.RolePlayingGamersAssociation;
            case "SEM - Seminar":
               return EventType.Seminar;
            case "SPA - Spouse Activities":
               return EventType.SpouseActivities;
            case "TCG - Tradable Card Game":
               return EventType.TradableCardGame;
            case "TDA - True Dungeon Adventures!":
               return EventType.TrueDungeonAdventures;
            case "WKS - Workshop":
               return EventType.Workshop;
            case "ZED - Isle of Misfit Events":
               return EventType.IsleOfMisfitEvents;
            default:
               throw new Exception(string.Format("Unable to convert event type \"{0}\"", text));
         }
      }
   }
}
