﻿#region Apache License 2.0

// Copyright 2015 Thaddeus Ryker
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#endregion

using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Org.Edgerunner.Gencon.EventRegistration.Utilities
{
   public static class TextUtilities
   {
      public static string CapitalizeWords(CultureInfo culture, string text)
      {
         // Creates a TextInfo based on the culture.
         var textInfo = culture.TextInfo;
         text = textInfo.ToTitleCase(text);
         // Now we have to fix ordinals since ToTitleCase mucks them up
         text = Regex.Replace(text,
                              @"\d+(St|Nd|Rd|Th)",
                              delegate(Match match)
                              {
                                 string v = match.ToString();
                                 return v.ToLower(culture);
                              },
                              RegexOptions.Compiled);
         return text;
      }

      public static bool IsCapitalized(CultureInfo culture, string text)
      {
         return (text == CapitalizeWords(culture, text));
      }

      public static string UpperCase(CultureInfo culture, string text)
      {
         return text.ToUpper(culture);
      }

      public static string LowerCase(CultureInfo culture, string text)
      {
         return text.ToLower(culture);
      }

      public static bool IsUpperCase(CultureInfo culture, string text)
      {
         return (text == UpperCase(culture, text));
      }

      public static bool IsLowerCase(CultureInfo culture, string text)
      {
         return (text == LowerCase(culture, text));
      }
   }
}