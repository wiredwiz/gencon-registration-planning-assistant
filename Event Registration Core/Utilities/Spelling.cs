﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Org.Edgerunner.Gencon.EventRegistration.Utilities
{
   /// <summary>
   /// Conversion from http://norvig.com/spell-correct.html by C.Small further edited by Thaddeus Ryker
   /// </summary>
   public class Spelling
   {
      protected static Regex WordRegex { get; private set; }
      protected Dictionary<string, int> Dictionary { get; private set; }
      protected Dictionary<string, string> WordMaps { get; private set; }
      protected CultureInfo Culture { get; private set; }

      /// <summary>
      /// Initializes a new instance of the <see cref="Spelling"/> class.
      /// </summary>
      static Spelling()
      {
         WordRegex = new Regex(@"\b[a-zA-Z]+", RegexOptions.Compiled);
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="Spelling"/> class.
      /// </summary>
      /// <param name="culture">Culture to use when spell checking</param>
      /// <param name="fileName">Name of the dictionary file to use.</param>
      public Spelling(CultureInfo culture, string fileName)
      {
         Culture = culture;
         string fileContent = File.ReadAllText(fileName);
         List<string> wordList = fileContent.Split('\n').ToList();
         Dictionary = new Dictionary<string, int>(wordList.Count);
         WordMaps = new Dictionary<string, string>(wordList.Count);         

         foreach (var word in wordList)
         {
            string trimmedWord = word.Trim().ToLower(Culture);
            WordMaps[trimmedWord] = word.Trim();
            if (WordRegex.IsMatch(trimmedWord))
            {
               if (Dictionary.ContainsKey(trimmedWord))
                  Dictionary[trimmedWord]++;
               else
                  Dictionary.Add(trimmedWord, 1);
            }
         }
      }

      /// <summary>
      /// Performs spelling correction on the specified text.
      /// </summary>
      /// <param name="text">The text.</param>
      /// <returns>The resulting corrected text.</returns>
      public string Correct(string text)
      {
         return WordRegex.Replace(text, delegate(Match match)
         {
            string word = match.ToString();
            if (TextUtilities.IsUpperCase(Culture, word))
               return word;
            if (TextUtilities.IsCapitalized(Culture, word))
            {
               var result = CorrectWord(word);
               if (!TextUtilities.IsLowerCase(Culture, result))
                  return result;
               return TextUtilities.CapitalizeWords(Culture, result);
            }
            if (!TextUtilities.IsLowerCase(Culture, word)) // This will mean mixed case, which likely means a name of some kind
               return word;

            return CorrectWord(word);
         });
      }

      /// <summary>
      /// Corrects the specified word.
      /// </summary>
      /// <param name="word">The word.</param>
      /// <returns>The corrected word.</returns>
      protected string CorrectWord(string word)
      {
         if (string.IsNullOrEmpty(word))
            return word;

         word = word.ToLower(Culture);

         // known()
         if (Dictionary.ContainsKey(word))
            return WordMaps[word];

         List<String> list = Edits(word);
         Dictionary<string, int> candidates = new Dictionary<string, int>();

         foreach (string wordVariation in list)
            if (Dictionary.ContainsKey(wordVariation) && !candidates.ContainsKey(wordVariation))
               candidates.Add(wordVariation, Dictionary[wordVariation]);

         if (candidates.Count > 0)
            return WordMaps[candidates.OrderByDescending(x => x.Value).First().Key];

         // known_edits2()
         foreach (string item in list)
            foreach (string wordVariation in Edits(item))
               if (Dictionary.ContainsKey(wordVariation) && !candidates.ContainsKey(wordVariation))
                  candidates.Add(wordVariation, Dictionary[wordVariation]);

         return WordMaps[(candidates.Count > 0) ? candidates.OrderByDescending(x => x.Value).First().Key : word];
      }

      private List<string> Edits(string word)
      {
         var transposes = new List<string>();
         var deletes = new List<string>();
         var replaces = new List<string>();
         var inserts = new List<string>();

         // Splits
         var splits = word.Select((t, i) => new Tuple<string, string>(word.Substring(0, i), word.Substring(i))).ToList();

         // Deletes
         for (int i = 0; i < splits.Count; i++)
         {
            string a = splits[i].Item1;
            string b = splits[i].Item2;
            if (!string.IsNullOrEmpty(b))
               deletes.Add(a + b.Substring(1));
         }

         // Transposes
         for (int i = 0; i < splits.Count; i++)
         {
            string a = splits[i].Item1;
            string b = splits[i].Item2;
            if (b.Length > 1)
               transposes.Add(a + b[1] + b[0] + b.Substring(2));
         }

         // Replaces
         for (int i = 0; i < splits.Count; i++)
         {
            string a = splits[i].Item1;
            string b = splits[i].Item2;
            if (!string.IsNullOrEmpty(b))
               for (char c = 'a'; c <= 'z'; c++)
                  replaces.Add(a + c + b.Substring(1));
         }

         // Inserts
         for (int i = 0; i < splits.Count; i++)
         {
            string a = splits[i].Item1;
            string b = splits[i].Item2;
            for (char c = 'a'; c <= 'z'; c++)
               inserts.Add(a + c + b);
         }

         return deletes.Union(transposes).Union(replaces).Union(inserts).ToList();
      }
   }
}
