﻿using System;
using CsvHelper.TypeConversion;

namespace Org.Edgerunner.Gencon.EventRegistration.Converters
{
   public class IntTypeConverter : ITypeConverter
   {
      #region ITypeConverter Members

      /// <summary>
      ///    Converts the object to a string.
      /// </summary>
      /// <param name="options">The options to use when converting.</param>
      /// <param name="value">The object to convert to a string.</param>
      /// <returns>
      ///    The string representation of the object.
      /// </returns>
      public string ConvertToString(TypeConverterOptions options, object value)
      {
         int actualValue = (int)value;
         return actualValue.ToString(options.CultureInfo);
      }

      /// <summary>
      ///    Converts the string to an object.
      /// </summary>
      /// <param name="options">The options to use when converting.</param>
      /// <param name="text">The string to convert to an object.</param>
      /// <returns>
      ///    The object created from the string.
      /// </returns>
      public object ConvertFromString(TypeConverterOptions options, string text)
      {
         if (string.IsNullOrEmpty(text))
            return 0;

         if (text.Trim().ToLowerInvariant() == "unlimited")
            return 9999;

         return int.Parse(text, options.CultureInfo);
      }

      /// <summary>
      ///    Determines whether this instance [can convert from] the specified type.
      /// </summary>
      /// <param name="type">The type.</param>
      /// <returns>
      ///    <c>true</c> if this instance [can convert from] the specified type; otherwise, <c>false</c>.
      /// </returns>
      public bool CanConvertFrom(Type type)
      {
         if (type == typeof(string))
            return true;

         return false;
      }

      /// <summary>
      ///    Determines whether this instance [can convert to] the specified type.
      /// </summary>
      /// <param name="type">The type.</param>
      /// <returns>
      ///    <c>true</c> if this instance [can convert to] the specified type; otherwise, <c>false</c>.
      /// </returns>
      public bool CanConvertTo(Type type)
      {
         if (type == typeof(int))
            return true;

         return false;
      }

      #endregion
   }
}