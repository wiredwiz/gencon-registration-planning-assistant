﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper.TypeConversion;

namespace Org.Edgerunner.Gencon.EventRegistration.Converters
{
   class GameSystemConverter : ITypeConverter
   {
      /// <summary>
      /// Converts the object to a string.
      /// </summary>
      /// <param name="options">The options to use when converting.</param><param name="value">The object to convert to a string.</param>
      /// <returns>
      /// The string representation of the object.
      /// </returns>
      public string ConvertToString(TypeConverterOptions options, object value)
      {
         return value.ToString();
      }

      /// <summary>
      /// Converts the string to an object.
      /// </summary>
      /// <param name="options">The options to use when converting.</param><param name="text">The string to convert to an object.</param>
      /// <returns>
      /// The object created from the string.
      /// </returns>
      public object ConvertFromString(TypeConverterOptions options, string text)
      {
         text = text.Trim().Replace("  ", " ");
         return text;
      }

      /// <summary>
      ///    Determines whether this instance [can convert from] the specified type.
      /// </summary>
      /// <param name="type">The type.</param>
      /// <returns>
      ///    <c>true</c> if this instance [can convert from] the specified type; otherwise, <c>false</c>.
      /// </returns>
      public bool CanConvertFrom(Type type)
      {
         if (type == typeof(string))
            return true;

         return false;
      }

      /// <summary>
      ///    Determines whether this instance [can convert to] the specified type.
      /// </summary>
      /// <param name="type">The type.</param>
      /// <returns>
      ///    <c>true</c> if this instance [can convert to] the specified type; otherwise, <c>false</c>.
      /// </returns>
      public bool CanConvertTo(Type type)
      {
         if (type == typeof(string))
            return true;

         return false;
      }
   }
}
