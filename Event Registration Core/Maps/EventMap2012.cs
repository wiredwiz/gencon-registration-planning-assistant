﻿using System;
using CsvHelper.Configuration;
using Org.Edgerunner.Gencon.EventRegistration.Converters;
using Org.Edgerunner.Gencon.EventRegistration.Data;

namespace Org.Edgerunner.Gencon.EventRegistration.Maps
{
   public sealed class EventMap2012 : CsvClassMap<EventFileListing>
   {
      /// <summary>
      ///    Initializes a new instance of the <see cref="EventMap2012" /> class.
      /// </summary>
      public EventMap2012()
      {
         Map(m => m.Id).Name("Game ID");
         Map(m => m.Group).Name("Group");
         Map(m => m.Title).Name("Title");
         Map(m => m.ShortDescription).Name("Short Description");
         Map(m => m.LongDescription).Name("Long Description");
         Map(m => m.EventType).Name("Event Type").TypeConverter<EventTypeConverter>();
         Map(m => m.GameSystem).Name("Game System");
         Map(m => m.RulesEdition).Name("Rules Edition");
         Map(m => m.MinimumPlayers).Name("Minimum Players").Default(0);
         Map(m => m.MaximumPlayers).Name("Maximum Players").Default(0);
         Map(m => m.AgeRequired).Name("Age Required");
         Map(m => m.ExperienceRequired).Name("Experience Required");
         Map(m => m.MaterialsProvided).Name("Materials Provided");
         Map(m => m.StartDateTime).Name("Start Date & Time");
         Map(m => m.Duration).Name("Duration").Default(0);
         Map(m => m.EndDateTime).Name("End Date & Time");
         Map(m => m.GmNames).Name("GM Names");
         Map(m => m.Website).Name("Website");
         Map(m => m.Email).Name("Email");
         Map(m => m.IsTournament).Name("Tournament?");
         Map(m => m.RoundNumber).Name("Round Number").Default(0);
         Map(m => m.TotalRounds).Name("Total Rounds").Default(0);
         Map(m => m.MinimumPlayTime).Name("Minimum Play Time").Default(0);
         Map(m => m.AttendeeRegistration).Name("Attendee Registration?");
         Map(m => m.Cost).Name("Cost $").Default(0);
         Map(m => m.Location).Name("Location");
         Map(m => m.RoomName).Name("Room Name");
         Map(m => m.TableNumber).Name("Table Number");
         Map(m => m.SpecialCategory).Name("Special Category");
         Map(m => m.TicketsAvailable).Name("Tickets Available").TypeConverter<IntTypeConverter>();
         Map(m => m.LastModified).Name("Last Modified");
      }
   }
}