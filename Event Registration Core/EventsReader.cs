﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;
using Org.Edgerunner.Gencon.EventRegistration.Data;
using Org.Edgerunner.Gencon.EventRegistration.Maps;

namespace Org.Edgerunner.Gencon.EventRegistration
{
    public static class EventsReader
    {
        public static List<EventFileListing> Read(string fileName)
        {
            var config = new CsvConfiguration();
            List<EventFileListing> events = new List<EventFileListing>();
            config.HasHeaderRecord = true;
            config.TrimFields = true;
            config.TrimHeaders = true;
            config.SkipEmptyRecords = true;
            config.RegisterClassMap<EventMap2012>();
            using (var reader = File.OpenText(fileName))
            {
                var csv = new CsvReader(reader, config);
                events = csv.GetRecords<EventFileListing>().ToList();
            }
            return events;
        }
    }
}